#############
Forum Terms and Conditions
#############


Any donation(s) or purchase(s) done to "Sunrise Roleplay” go towards the costs of operating the "Services". "Sunrise Roleplay" is not cheap to maintain and all donation(s) or purchase(s), large and small, are very much appreciated by the Sunrise Roleplay Administrative Team.

Donations by users have made Sunrise Roleplay possible and they allow us to cover our costs and put our time and effort into making our services even better. If you have other ways you think you can help us out, feel free to contact us.

These Terms and Conditions are applicable to any donation(s) or purchase(s) made at any date, and with any amount donated or paid. We reserve the right to update these terms at any time, with or without notice to you. By donating or purchasing a product, you signify your acceptance of these terms and conditions and you agree to follow the rules and terms of service for any “Sunrise Roleplay” “Services” you use.

Last Update: 06/13/2018


Definitions
===========
"Sunrise Roleplay" - The term "Sunrise Roleplay" refers to Sunrise Roleplay as a whole, which is represented by the Sunrise Roleplay Administrative Team.

"Services" - The term "Services" refers to any service that Sunrise Roleplay offers, which includes our FiveM server, Website, Forums, etc.

"Membership" - The term "Membership" refers to an active subscription that Sunrise Roleplay offers, which includes all of our “Services”


Donation Values
===============
Stated donation values are in US Dollars (USD).


Donation Methods & Process
==========================
Donations are accepted through our Sunrise Roleplay store via Stripe or PayPal, which allows quick and automatic processing.


Processing Time
===============
Donations & Purchasing made through Stripe or PayPal are handled automatically by our system. If you purchased a product, the order should be applied within 30 seconds after the purchase is made (unless stated otherwise). For virtual items to take effect on our “Services” you may have to reconnect from the “Services” for it to take effect. More details can be found in the “Services” forums section that you’re attending to. However, if the virtual items aren’t received within 5 minutes period prior to purchase we highly recommend you to open a support ticket or contacting a “Services” administrator.


Payment Rules
=============
**LEGITIMACY OF PAYMENTS**

Donation(s) and purchase(s) must be made with the expressed authorization of the account holder or cardholder. Donation(s) and purchase(s) not made with expressed authorization are not welcomed by “Sunrise Roleplay” and if issues arise “Sunrise Roleplay” reserves the right to remove privileges and restrict or remove the user’s access to “Sunrise Roleplay” and its “Services”.


**REFUNDABILITY**

Payments made to “Sunrise Roleplay” are non-refundable unless stated otherwise. “Sunrise Roleplay” will always endeavor to provide a satisfactory outcome as is judged best by the Administrative Team for all users and “Sunrise Roleplay” as a whole. We attempt to be as transparent as possible in our processes and in what you should expect. Even if a Donator is still not satisfied with an outcome, a refund should not be expected, nor will it be given.


**APPLICATION OF SERVER RULES**

All “Sunrise Roleplay” rules and terms of service still apply to Donators and “Memberships”. In the event a Donator or “Membership” user has their privileges or access to an “Sunrise Roleplay” service restricted for a breach of the rules or terms of service, they will be given a fair chance to have their concerns heard, as any normal user would have. Donators or "Membership" users will not be given special consideration or treatment because they have made a donation or uphold a valid “Membership”.


**MEMBERSHIP**

“Memberships” purchased by any users are an active subscription. This subscription is non-refundable unless stated otherwise. “Sunrise Roleplay” will always endeavor to provide a satisfactory outcome as is judged best by the Administrative Team for all users and “Sunrise Roleplay” as a whole. Even though the subscriptions are automatically renewal, it is the account holder’s responsibility to terminate the subscription at the point they no longer want it. If a chargeback or dispute via Stripe we offer no refunds back, the reason for this is because we do not want to involve Stripe with nonsense whilst we prefer to resolve it ourselves. If you wish a refund we can agree to do so if a ticket is submitted to our ticket center. However, keep in mind that there are still no guarantees that we will refund your money back, if we should decide to do so we offer only the last months money back unless an extremely good explanation is submitted to us.


Privileges Awarded To Donators
==============================
In return for a donation to “Sunrise Roleplay”, the Donator may receive a donator rank within our websites and voice services. This is done as a way to thank users who donate and to recognize their instrumental support in helping to maintain “Sunrise Roleplay”. These privileges are non-transferrable, and the perks, names, and weight of ranks may change at any time. These perks are not meant to make a big difference between regular users and those who have the donator tags. If a user has a capacity to store 500kb pictures on our forums, donators, for example, may have 1mb storage. There are no restrictions on what a donator and a user can do, only size limitations, etc.


**INGAME PRIVILEGES**

In return for “Membership” holders or purchase(s) made to “Sunrise Roleplay”, users may receive a set of privileges that correspond to the “Membership” or purchase made. The official list of the entitlements for each purchase can be found in the products described in our store. “Sunrise Roleplay” will do its best to honor the privileges it specified to a user at the time the “Membership” or purchase were made. However, this is not always possible and “Sunrise Roleplay” reserves the right to make changes to the reward scheme and “Sunrise Roleplay’s services” as “Sunrise Roleplay” sees fit.

“Sunrise Roleplay” reserves the right to remove any privileges granted at any time for any reason.


**CUSTOM DONATION**

You also have the right to refuse and privileges received from donations made to “Sunrise Roleplay”, we aren’t forcing ranks and titles on any of our members. However, our systems are automatically detecting donations and apply your granted privileges. If you should NOT want those granted privileges we ask you to submit a ticket to our ticket center and one of our staff members will remove it as soon as possible.


**SERVICE CHANGES**

Changes may occur, such as a server reset, data corruption, or user error, that could remove privileges or affect gameplay. We will do our best to restore privileges for current subscriptions and active users in good standing, but some items may be irretrievable.


**LIABILITY**

“Sunrise Roleplay” will not be held responsible for any harm caused to its users through the use of its “Services”.


Indemnification
===============
You agree to fully indemnify, hold harmless and defend (collectively “indemnify” and “indemnification”) “Sunrise Roleplay” and its directors, officers, employees, agents, stockholders and Affiliates (collectively, “Indemnified Parties”) from and against all claims, demands, actions, suits, damages, liabilities, losses, settlements, judgments, costs and expenses (including but not limited to reasonable attorney’s fees and costs), whether or not involving a third party claim, which arise out of or relate to (1) any breach of any representation or warranty of “Sunrise Roleplay” contained in this Agreement, (2) any breach or violation of any covenant or other obligation or duty of “Sunrise Roleplay” under this Agreement or under applicable law, in each case whether or not caused by the negligence of “Sunrise Roleplay” or any other Indemnified Party and whether or not the relevant Claim has merit.


Authorization
=============
Continued use of this website and other “Sunrise Roleplay” “Services” and donating or purchasing to “Sunrise Roleplay” indicates agreement with these Terms and Conditions and you further agree that:

You are above the age of 16 or 16 years of age, or you are operating with the guidance and consent of a parent or legal guardian.

You have the authorization to use the debit/credit card to donate or purchase.

By making a donation to “Sunrise Roleplay” you are not entitled to any tangible goods.

You will not chargeback, dispute, or otherwise, reverse any payments.

You will not abuse and/or misuse your reward(s).

We reserve the right to terminate any account without notice for any reason.

You understand there are no refunds and you will not be issued a refund.

You agree to be bound by these Terms and Conditions as listed above.

If you do not agree to the terms listed above then do not donate or purchase a product.

Failure to comply with any of these Terms and Conditions can lead to a permanent termination/ipban from all of our “Services”.