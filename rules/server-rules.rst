############
Server Rules
############

.. note:: All players are required by the community and the management to read through this agreement and the rules when joining the Sunrise Roleplay Server. You are responsible for checking this rules list for any updates (**Last Update:** 2019-08-17 5:38 PM GMT). Rule may be updated at any time, it is your job to keep updated with them when joining the server it is implied that you accept these terms as well as you following them. 
	
	**By entering any of our community servers you agree to the following set of rules along with all rules for that individual server or site. You also agree, upon entering any community server​s, to accept any punishments for breaking said rules. You agree that you are responsible for the actions committed on your account during your visit.**

General Rules
=============

1) Please remain in character at all times in game. Breaking character via voice is not  tolerated. (Using voice chat or tweets for out of character information is prohibited.)
	a) In Character means that you are acting as your character. You are taking in the in-game world in through the eyes,ears and mind of your character. You are not acting as yourself. It’s important to make this distinction. 
	b) Out of Character refers to your responses to people as yourself rather than your character. Being out of character means that you are not role playing. Out of character communication is limited to /ooc and local chat (white text). 
	
2) No Meta-Gaming. (Using info that you have gained out of character for an In-Character benefit) Metagaming refers to when a player uses knowledge they’ve obtained through OOC means in the IC world to gain an advantage. Examples of Metagaming include:
	a) Seeing another player’s discord gang tag and using that IC to know someone else’s character belongs to a gang.
	b) Hearing a story about a role play that’s happened while in a lounge channel in discord and then making your character privy to the information gained by hearing said story. 
	c) Asking “how many cops are on?” in /ooc before committing a crime.
	
3) No Power Gaming. (Powergaming covers three different parts of RP; RPing things that are impossible or unrealistic, forcing your role play onto another player, or making up RP scenarios for the benefit of your character without any of the negative consequences.) Examples include:
	a) Using /chance or a /me to turn a losing roleplay scenario into a win. ie: “/me breaks handcuffs.” Or “/me tears through rope ties to pull a hidden weapon.”
	b) Using /me to do actions that make no sense in the role play, such as: “/me ties the man up.” While you are on the other side of the room. 
	c) Forcing another character to comply with all biddings and actions without a chance to resist.  /chance does not overrule this.
	d) Giving your character supernatural powers.
	e) Using knowledge of the rules to stop, avoid, or manipulate a RP.
	
4) No FailRP (Doing actions that are impossible In Real Life to gain an advantage in roleplay unfairly.) For example:
	a) Escaping the police in a chase by flying your car off a mountain.
	b) Riding a tuned supercar up a rocky mountain road at 200mph.
	c) Dodging and trying to outrun bullets.

5) You must value your life at all given times, an average person with things to lose will nearly always value there life over there belongings just as you would likely do in real life so to should your character when presented with the opportunity
	a) Put yourself in the shoes of your character if that gun was in your face IRL you’d hand over your wallet without a second thought
	b) In a shootout bleeding, you would beg for medical attention not try and do all you could to die so you can escape justice, real life has no respawn and your character does not know he/she is going to survive these wounds

6) No VDM (Vehicle DeathMatch is when you attempt to use your vehicle as a weapon against another player without reason or roleplay behind it)
	a) Ramming your car into another players vehicle without any RP interaction is VDM.
	b) Running over pedestrians without any RP is VDM. 

7) No RDM (Random DeathMatch is when you attack or kill another player without reason or roleplay behind it)﻿
	a) Running up and punching a player in the face without any reason or initiation is RDM. 
	b) Shooting at a group of people with a sniper without being initiated into the RP is RDM.

8) If your character is executed during an RP scene your character may not be re-initiated into the scene after respawning. For example:
	a) A gang-on-gang shootout is taking place. If your character is killed during the shootout and respawns at the hospital, your character may not rejoin that role play scenario. It is not realistic as they have been ‘killed’ in that RP.

9) No Fail Driving (Driving your vehicle in areas it wouldn't survive IRL)
	a) Driving a vehicle not suitable for the terrain is fail driving. Ex: Driving a sports car up a mountain. 
	b) Continuing to drive after a high speed impact is fail driving. Realistically the car and your character would be injured.

10) **What happens in RolePlay stays in RolePlay.(Do not bring in character arguments or love affairs into real life you are not your character and he/she is not you keep it seperate)**

11) **No advertising anything unless given explicit permission from an Admin or above**

12) **All forms of communication in the game shall remain in English. This includes, but is not limited to voice chat, OOC, and Twitter.**

13) No internal/external mods or hacks. 
	a) No crosshairs
	b) No mod menus
	c) No third-party programs that provide any sort of advantage over other players.

14) **Do not abuse bugs or use game limitations to gain an advantage.**

15) **No Rape RP (All ERP must be consensual from both sides if it is not then it is explicitly prohibited)**

16) **No Terrorism RP (Deliberately creating a roleplay to strike terror into many such as stealing a plane and threatening to crash it into a high population area for no real reason)**

17) No blatant racism (In character or out of character). This includes but is not limited to:
	a) Hate Speech is not tolerated.
	b) No harmful or directed stereotyping. Be respectful.

18) No Homophobia (Dislike of or prejudice/hate speech against homosexual people).
	a) No harmful or directed stereotyping. Be respectful.
	b) Hate speech is not tolerated.

19) **Microphones are a requirement**

20) **You are not allowed to trade in-game items/currency for anything IRL outside of the Members Shop on the website**

21) **No excessive sexual harassment (if asked to stop OOC please respect their boundaries.)**

22) **Using an invalid date of birth that doesn't match the format YYYY-MM-DD or isn't a realistic DoB is strictly forbidden. (Time Travel RP is allowed but might be treated as a fake ID by the police)**

23) **No AFK Farming in drug locations for longer than 5 minutes (coffee break.)**

24) All jobs received at the job center must be completed in the work vehicle provided at each job's locker room. Doing these jobs in a personal/NPC vehicle is against the rules.
	a) The provided work vehicles are immersive and are capable of carrying the work materials. For example, another smaller car wouldn’t realistically be able to carry the logs for the lumberjack job or the rocks from the miner job. 
	b) In order to maintain balance everyone must complete the job center jobs in the same vehicle. 

25) You can not run to a private/invite-only instance in the middle of a RolePlay scenario.
	a) This specifically is there because unlike in irl where that person could follow you into the property in FiveM it puts you in a protective bubble so whether it is with another civilian or a cop it is not allowed unless both parties agree someone can enter.

26) **Whilst you are allowed to enter the military base you are NOT allowed to interact with any military equipment. (such as but not limited to Fighter Jets and Tanks.**

27) All in character voice communication must be spoken in-game at all times. When using out-of-game voice communications, such as a bluetooth call using discord, you must also speak in game in at least “normal” range. It is unrealistic that people nearby wouldn’t be able to hear words spoken into a bluetooth. Examples include:
	a) You have been pulled over by a police officer. Your window is rolled down and the police officer is stood next to your door.  You are in a bluetooth call with your buddies and would like to call them to back you up. While you speak in discord you are **required** to simultaneously speak in game. This can be done by holding down your in-game push to talk as you speak in discord. 
	b) You are on a criminal meet, during the scenario you grow suspicious and wish to speak to your friend in bluetooth. While within normal range of the other party, you may not communicate privately within a bluetooth call. 
	c) These rules also cover walkie-talkies, wireless(satellite) headsets and various other forms of wireless communication.
	
27.5) You are required to have a physical item in your ear to be allowed to use Bluetooth communications, for example: Earpieces or helmet with microphones.

28) **Please keep OOC chat to a minimum, No arguing in OOC chat. if you feel someone has broken server rules please put in a /report (Players ID that you are reporting)(Why you are reporting them). If there is no admin available to take the report in real time please visit `https://forums.sunrise-roleplay.eu/ticket-center/ <https://forums.sunrise-roleplay.eu/ticket-center/>`_ and fill out an in-game-support ticket.**

29) **You are not allowed to identify someone by their voice. If you hear a person's voice without a mask, then encounter them later while they’re wearing a mask your character doesn’t automatically know who the person is based on their voice**


Green Zone Rules
================

1) No crimes are to be committed within Green Zones. Crimes that were started outside of a Green Zone may be carried out if the scene moves into a Green Zone. For example, 
	a) You are in a police chase and go into the public garage. The police may still pursue you as the RP started outside of the Green Zone.

2) No Green Zone Baiting. Green Zone baiting is starting or provoking someone within a green zone knowing that it may result in violence, theft, or any crime. For example:
	a) You are in the green zone and someone starts verbally threatening your life. This is green zone baiting. 
	b) You are standing within a green zone and tweeting out sensitive information about another character/sending threats via twitter. This is green zone baiting.

3) **Whilst within a designated safe zone or green zone such as PD or the hospitals you are not allowed to release information about a character via twitter using the zone as a shield to avoid consequences such as but not limited to violent retaliation by the victimised party, this includes both public and private release of information but does not include a police investigation you may also not initiate on anyone via twitter in a greenzone**


Police Rules
============

1) **Cop baiting without a role play reason is not tolerated. Ramming police cars because you’re bored is not allowed. Luring a police officer to take them hostage for a role play scene is allowed**

2) Players that are part of LSPD or BCSO may not have a second character that is a member of a gang.
	a) Noticed Role Players may apply to have a second character who can join an official gang. 
	b) Players who RP a gang leader may not have a character in LSPD/BCSO.
	
3) **No stealing Emergency Service Vehicles unless you've been granted access by an Admin within RP.**

4) **Police are NOT allowed to plate check or arrest or start any form of police on civilian RP within the 5 public garages. If the RP began outside the public garage this is not relevant. Police may question for information but not arrest, detain, search, cuff, taze or shoot under any circumstances if someone else does its a green zone violation call an admin**

5) **Cell phones cannot be used when handcuffed, or after they have been removed in RP, or if you are incapacitated. Please see #📌death-rules for more information.**


EMS Rules
=========

1) **Medics should not be within 200 meters of an on-going shooting, Reviving during active-combat is not allowed. No active-combat defibs either.**

2) Characters that are part of LSFD may not be in a gang. This is to prevent an unfair advantage based on script limitations. 
	a) Preventing rival gangs from using defibs by going on duty during or as a direct result of a scenario.

3) **No stealing Emergency Service Vehicles unless you've been granted access by an Admin within RP.**

4) **In order to take an EMS hostage there must be at least 3 ems on duty or OOC consent must be given by said EMS**


Whitelist Rules
===============

1) If you call/text any Whitelist business you must provide a detailed message of what you're needing. 
	a) To EMS: I've been in a car accident.
	b) To Car Dealer: I'm looking to buy a car. Are you open?

2) **While communicating with (or as) a business via phone, you may not add or write down a players phone number. Realistically, each business would have their own phone line but due to FiveM limitations, players are forced to use their own phones and phone numbers to reply. Taking down numbers after players have texted a business number is considered meta and powergaming. *All government institutions are exempt from this rule as they are not business**

3) **Car dealers are not to use any cars from the dealership for personal use if found doing so there job will be removed. (This is here to stop an unfair advantage of unlimited cars someone can't afford as well as the realistic point of your putting miles on a new car and its no longer saleable as new you’d be fired at best sued and jailed for fraud at worst**

4) **You may NOT abuse any whitelisted position in any way, whitelisted jobs have a key to making a successful server. If a whitelisted job no longer functions properly it may cause potential issues for the server and the community.**

Death Rules
===========

1) **When doing Hitman RP you must do the following. You must notify the person that you are a hitman who has been hired to end their life. This must be done before the person has been attacked/injured and does count as a form of initiation.**

2) **Executions must be realistic. Your character must realistically be able to perform the action and have the tools to match the /me that is written (ex: to slit a throat you must have a knife.)**

3) **If you get executed at an illegal location(drugs locations,etc), You can not return to said location for 1 hour after respawning.**

4) New Life Rule:  Upon ‘death’, your character forgets the events leading up to their death and after respawning, you can't go back to your area of death for at least 15 minutes. **Exceptions:**
	a) In the case of an accidental death (forgetting to eat “pulling an EMILY”, motorcycle accident, falling from too high, drowning) your character is not required to forget the events leading up to their death and they may immediately return to the RP scene.
	b) If your character is executed at an illegal location (drug location, black market, etc) your character may not return to said location for 1 hour. 
	c) If a witness survives, or someone finds and inspects your body their character can inform your character of what they know
	

Combat Rules
============

1) The driver of all forms of vehicles may never shoot from their vehicle unless completely stationary. Passengers, however, may fire at-will so long as all other RP requirements have been met(ex: initiation has happened).
	a) GTA makes this extremely easy to do as the game was designed for casual npc slaying in IRL that is very difficult to accomplish and at best extremely inaccurate and likely to end in the driver crashing
	
2) **Initiation Timer - When you initiate on someone, that initiation is valid for 15 minutes ( ex: you are chasing someone lose them and see them an hour later you MUST reinitiate).**

3) If a player is being compliant during a robbery they can not be killed unless there is dirty money, drugs or some form of an illegal transaction involved. For example:
	a) You come to the gym and rob someone who is working out for their cash, you can’t not kill them at the end of the robbery if they’ve complied with your orders. 
	b) You meet up with someone who is looking to purchase drugs from you. Instead, you rob them. In this scenario you may kill them.

4) You can not initiate crime on workers that are doing non-whitelist jobs. They must be in a work uniform with a work vehicle. For example, 
	a) You cannot rob a tailor at the wool collection job location if they are wearing their work uniform and their work van is parked nearby. 
	b) You can rob a person who is at the wool collection that is not using the correct vehicle.
	c) You can initiate crime on a person who is at the butcher job who is not wearing the job uniform.

5) In order to restrain someone or remove their weapons in RP they must be compliant. You can’t simply do a /me takes someone's weapon and expect it to be relevant unless they are being forced to value their life or doing it voluntarily during RP﻿. For example, 
	a) if you’ve asked someone to submit to a pat down, or have asked them to hand over all of their weapons you can reasonably take the items if the other party complies.

6) OOC Consent from all parties involved must be obtained to participate in graphic and excessively violent RP. This includes but is not limited to:
	a) Heavy torture role play.
	b) Detailed /me’s involving dismemberment or disfigurement.
	c) ERP and any RP of overtly sexual nature.
	d) Consent may be revoked at anytime during the scene.
	e) If consent is not granted the scene can fade-to-black -- a sort of “we’ll say it happened” time-skip.  

7) Refusing consent does not grant your character immunity from combat RP. For example:
	a) In the case you are kidnapped and are being tortured, refusal to consent to graphic RP doesn’t mean that the RP will stop and your character will be unharmed. 
	b) If you initially granted permission to participate in graphic RP and revoke consent mid-RP the scene doesn’t stop or restart. It merely switches to a fade-to-black or a significantly scaled down scene. 


Quality of Life Rules
=====================

1) **You are not allowed to use public garages to collect license plates or information on other player’s vehicles. Unlike real life, every player is forced to get their cars from these locations and using this to gain an in-game advantage is powergaming.**

2) Stolen cell phones can not be used to acquire information about the character in-game. This rule is in place to stop potential metagaming via discord gang tags, or information acquired out of character. You can, however, force them to either give you a single contact if you already know they have it in character or delete your own number from their phone. For example
	a) While kidnapping someone, you cannot take their phone and then force them to give you the phones password so that you can read all their texts and get all their contacts. 
	b) Your character is looking for information about Character A. Your character hears character X mention texting Character A. As your character now knows that Character X has Character A’s number, your character may now steal Character X’s phone to acquire Character A’s number. 
	c) Your number has fallen into the hands of someone you don’t want to have it. If you manage to get this person’s phone you are allowed to remove your number from their phone.
	d) Information can be shared via mutual consent. Characters may voluntarily, while not coerced or under duress, share their texts, contacts, and any other information acquired fairly within roleplay that would be stored in their phone.

3) **You can not force someone to go to an ATM and give you all their money. Whilst you could take them to an ATM in real life, you’d be unable to get all of their money as real life ATMs have a limit on the amount of cash you can withdraw in a day. This also applies to bank transfers.**

4) If your character ‘leaves’ for an extended period of time (2 weeks or more), you must first notify your gang or criminal friends that you are leaving. Upon returning, your character cannot immediately use their criminal knowledge to detrimentally affect storylines without first letting their gang/criminal friends know that they are back. Informing criminal ties of your return starts a 3-day grace period to allow for the facilitation of storyline in a fair way.  During this 3-day grace period you must play your character somewhat regularly. This rule is in place to prevent someone from leaving on bad terms, staying away to avoid in-character consequences and then returning for one day for the sole purpose of throwing their criminal buddies under the bus. Examples include:
	a) Someone has an OOC argument with a friend or gang member and leaves the server. Days or weeks pass and he comes back with the purpose of getting OOC revenge.
	b) The RP has gotten risky for your character. You may not take a long break to avoid RP consequences and then return to disrupt the role play as a counter-play.