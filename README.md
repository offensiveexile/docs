# Sunrise Roleplay Community Documentation. [![Documentation Status](https://readthedocs.org/projects/sunrise-roleplay/badge/?version=latest)](https://sunrise-roleplay.readthedocs.io/en/latest/?badge=latest)

The Sunrise Roleplay Community Documentation.

This can be found at https://docs.sunrise-roleplay.eu/

If working on locally you can build the environment by using `sphinx-autobuild`

### Windows

```
pip install -r requirements.txt
pip install sphinx-autobuild
make html
```

### Linux/MacOS

```
pip install -r requirements.txt
pip install sphinx-autobuild
./Makefile html
```
